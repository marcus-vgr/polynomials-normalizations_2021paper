# Polynomials-Normalizations

The .m file contain the Polynomials $`P_n^\delta`$ and the Normalizations $`N_n^\delta`$ of the paper

D. Boito, V. Mateu, M. V. Rodrigues, "Small-momentum expansion of heavy-quark correlators in the large-$`\beta_0`$ limit and $`\alpha_s`$ extractions", Pre-prints: UWThPh 2021-6 and IFT-UAM/CSIC-21-70

in a form readable for Mathematica.
